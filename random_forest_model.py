#!/usr/bin/env python2

##This script predict AMR based on the aligned fragments or aligned whole genome data.
##Antibiotic name and fragment lengths is required.
##if the model should be trained for fragments, make sure the correct parameter -f is given.
##All the requred functions and extra python libraries were loaded.
##Random seed set to default value 42. 

import matplotlib
matplotlib.use('Agg')
import random
import numpy as np
import collections
from sklearn import preprocessing
from keras.utils import to_categorical
from numpy import argmax
from sklearn.metrics import roc_curve, auc, f1_score, accuracy_score, matthews_corrcoef
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import KFold
import matplotlib.pyplot as plt
import argparse
import fragments ##all the custom functions

if(__name__ == "__main__"):

	random.seed(42)

	parser = argparse.ArgumentParser()

	parser.add_argument("-i", "--input", required = True, help = "List of the isolates required")
	parser.add_argument("-d", "--directory", default = ".", help = "Path to the aligned isolates directory")
	parser.add_argument("-m", "--meta", required = True, help = "Metadata")
	parser.add_argument("-r", "--res", required = True, type = str, help = "Value for resistant isolates")
	parser.add_argument("-u", "--sus", required = True, type = str, help = "Value for susceptible isolates")
	parser.add_argument("-a", "--anti", required = True, help = "Antibiotic name desired to predict")
	parser.add_argument("-f", "--frag", type = int, help = "Fragment size, default is the whole genome")
	parser.add_argument("-c", "--clus", type = str, help = "For clustering, provide clustering results (whole alignment only)")
	parser.add_argument("-k", "--kfold", type = int, help = "Number of folds in CV")

	args = parser.parse_args()

	fasta_names_all = np.loadtxt(args.input, dtype = "string")

	data_y_all = np.loadtxt(args.meta, dtype = "string", delimiter = "\t")

	data_y_ant = fragments.export_meta(data_y_all, "%s" % args.anti) ##export the only the desired antibiotic from the metadata

	fasta_names, data_y_dict = fragments.meta(data_y_ant, fasta_names_all, args.res, args.sus) ##prepare metadata and the isolates having metadata

	fasta_list, data_z, wg_size  = fragments.read_data(fasta_names, args.directory) ##read the files, remove the empty isolates from the list

	data_y = fragments.prep_y(data_y_dict, data_z) ##final true output data

	del data_y_dict ##delete the unnecessary objects

	##Number of fold in CV

	cv = args.kfold

	##if isolate clustering is required, open the clusters

	if args.clus is not None:

		clusters_open = open("%s" % args.clus, "r")
		clusters = clusters_open.readlines()

		##clustered samples
		cluster_dict = fragments.clusterm(clusters)

	##assess and print the prediction performances in AUC, F1, accuracy, MCC, ME, VME

	##Find out the script is for fragments or WG

	if args.frag is not None:
		fragment_size = args.frag

	else:
		fragment_size = wg_size

	##Sampling should be repeated until the total lentght of the fragments cover the whole genome in theory 	

	sampling_size = int(wg_size/fragment_size)

	temp_performances_test = []
	temp_performances_val = []

	temp_performances_test_f1 = []
	temp_performances_val_f1 = []

	temp_performances_test_acc = []
	temp_performances_val_acc = []

	temp_performances_test_mcc =  []
	temp_performances_val_mcc = []

	temp_performances_test_me = []
	temp_performances_val_me = []

	temp_performances_test_vme = []
	temp_performances_val_vme = []

	for s in range(sampling_size):

		if args.frag is not None:

			start = fragments.find_region(len(fasta_list[data_z[0]]), fragment_size)

		else:
			start = 0

		f_matrix = fragments.matrix(fasta_list, data_z, start, fragment_size)

		f_matrix_snp = fragments.snp_matrix(f_matrix)

		#print(f_matrix_snp.shape)

		if f_matrix_snp.shape[1] == 0:
			continue 

		del f_matrix

		encoded_data_x = to_categorical(f_matrix_snp)

		encoded_data_x = np.reshape(np.ravel(encoded_data_x), (f_matrix_snp.shape[0], f_matrix_snp.shape[1]*encoded_data_x.shape[2]))

		del f_matrix_snp

		##machine learning 

		##validation samples

		if args.clus ==  None:

			##validation samples

			rand_ind = random.sample(range(len(data_y)), int(len(data_y)/6))

			##remaining dataset 

			remain = list(set(range(len(data_y))) - set(rand_ind))

			##CV with sklearn 

			kf = KFold(n_splits = cv, random_state = 42, shuffle = True)

			##training data

			data_x_test = encoded_data_x[remain]
			data_y_test = np.array([data_y[i] for i in remain])

			##indexes 

			train_indexes = []
			test_indexes = []

			for train_index, test_index in kf.split(data_x_test):
				train_indexes.append(train_index)
				test_indexes.append(test_index)

		else:
			##validation samples 

			rand_clusters = random.sample(cluster_dict.keys(), int(len(cluster_dict.keys())/6))

			rand_ind = []
			for i in rand_clusters:
				for element in cluster_dict[i]:
					if element in data_z:
						ind = data_z.index(element)
						rand_ind.append(ind)


			##remaining clusters and samples
			remaining_clusters = list(set(cluster_dict.keys()) - set(rand_clusters))

			##custom CV

			training_data_clustered = []

			r = int(len(remaining_clusters)/cv)
			a = 0
			b = 0
			for i in range(cv):

				if i != cv - 1:
					a = a
					b = a + r
				else:
					a = a
					b = len(remaining_clusters)

				training_data_clustered.append(remaining_clusters[a:b])
				a = a + r

			##remaining samples and indexes 

			training_data_clustered_ind = []
			for c in training_data_clustered:
				temp = []
				for element in c:
					for item in cluster_dict[element]:	
						if item in data_z:
							ind = data_z.index(item)
							temp.append(ind)
				training_data_clustered_ind.append(temp)


		val_data_x = encoded_data_x[rand_ind]
		val_data_y = [data_y[i] for i in rand_ind]

		##outputs

		val_probs = []

		val_classes = []

		all_aucs = []

		for i in range(cv):

			if args.clus == None:

				train_data_x, test_data_x = data_x_test[train_indexes[i]], data_x_test[test_indexes[i]]
				train_data_y, test_data_y = data_y_test[train_indexes[i]], data_y_test[test_indexes[i]]

			else:

				test_index = training_data_clustered_ind[i]

				remain_fold = list(set(range(cv)) - set([i]))

				train_index = []

				for c in remain_fold:
					for item in training_data_clustered_ind[c]:
						train_index.append(item)

				train_data_x, test_data_x = encoded_data_x[train_index], encoded_data_x[test_index]
				train_data_y, test_data_y = np.array(data_y)[train_index], np.array(data_y)[test_index]

			##Random Forest model

			clf_1 = RandomForestClassifier(n_estimators = 200, random_state = 42, class_weight = "balanced", n_jobs = 4)
			clf_1.fit(train_data_x, train_data_y)

			test_prob = clf_1.predict_proba(test_data_x)[:,1]

			val_prob = clf_1.predict_proba(val_data_x)[:,1]

			test_class = clf_1.predict(test_data_x)

			val_class = clf_1.predict(val_data_x)

			val_probs.append(val_prob)

			val_classes.append(val_class)

			##Test performances

			##AUC

			fpr, tpr, _ = roc_curve(test_data_y, test_prob, pos_label = 1)

			roc_auc = auc(fpr, tpr)

			temp_performances_test.append(roc_auc)

			##F1

			f1 = f1_score(test_data_y, test_class, average='macro')

			temp_performances_test_f1.append(f1)

			##Accuracy score

			acc = accuracy_score(test_data_y, test_class)

			temp_performances_test_acc.append(acc)

			##MCC

			mcc = matthews_corrcoef(test_data_y, test_class)

			temp_performances_test_mcc.append(mcc)

			##Major error

			me = fragments.ME(test_data_y, test_class, 1, 0)

			temp_performances_test_me.append(me)

			##Very Major error

			vme = fragments.VME(test_data_y, test_class, 1, 0)

			temp_performances_test_vme.append(vme)


		##Validation performances
		mean_val_prob = np.array(val_probs).sum(axis = 0)/float(cv)
		mean_val_class = fragments.class_finder(mean_val_prob, 0.5, 1, 0)

		##AUC
		fpr_val, tpr_val, _ = roc_curve(val_data_y, mean_val_prob, pos_label = 1)
		roc_auc_val = auc(fpr_val, tpr_val)
		temp_performances_val.append(roc_auc_val)

		##F1
		f1_val = f1_score(val_data_y, mean_val_class, average='macro')
		temp_performances_val_f1.append(f1_val)

		##Accuracy
		acc_val = accuracy_score(val_data_y, mean_val_class)
		temp_performances_val_acc.append(acc_val)

		##MCC
		mcc_val = matthews_corrcoef(val_data_y, mean_val_class)
		temp_performances_val_mcc.append(mcc_val)

		##ME
		me_val = fragments.ME(val_data_y, mean_val_class, 1, 0)
		temp_performances_val_me.append(me_val)

		##VME
		vme_val = fragments.VME(val_data_y, mean_val_class, 1, 0)
		temp_performances_val_vme.append(vme_val)


	##Print and visualize prediction performances

	tags = ["AUC", "F1", "ACCURACY", "MCC", "ME", "VME"]

	test_items = [temp_performances_test, temp_performances_test_f1, temp_performances_test_acc, temp_performances_test_mcc, temp_performances_test_me, temp_performances_test_vme]

	val_items = [temp_performances_val, temp_performances_val_f1, temp_performances_val_acc, temp_performances_val_mcc, temp_performances_val_me, temp_performances_val_vme]

	for i in range(len(tags)):
		mean, std = fragments.mean_std(test_items[i])
		#bar_plot(mean, std, "%s performances" % tags[i], "Validation_performances_%s_%s" % (tags[i], ant_name)) ##the paper uses the notation differently

		print(tags[i], "cv data")
		print("mean", mean)
		print("std", std)

		mean_2, std_2 = fragments.mean_std(val_items[i])

		#bar_plot(mean_2, std_2, "%s performances" % tags[i], "Test_performances_%s_%s" % (tags[i], ant_name))


		print(tags[i], "hold out data")
		print("mean", mean_2)
		print("std", std_2)
		print("##########")


