#!/usr/bin/env python2

##This script predicts AMR from fragments. 
##The model was trained and tested by the distinct fragments. 

import os
os.environ["OMP_NUM_THREADS"] = "2" 
import random
import numpy as np
import collections
from sklearn import preprocessing
from keras.utils import to_categorical
from sklearn.metrics import roc_curve
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import KFold
import argparse
import fragments ##all the custom functions

if(__name__ == "__main__"):

	random.seed(42)

	parser = argparse.ArgumentParser()

	parser.add_argument("-i", "--input", required = True, help = "List of the isolates required")
	parser.add_argument("-d", "--directory", default = ".", help = "Path to the aligned isolates directory")
	parser.add_argument("-m", "--meta", required = True, help = "Metadata")
	parser.add_argument("-r", "--res", required = True, type = str, help = "Value for resistant isolates")
	parser.add_argument("-u", "--sus", required = True, type = str, help = "Value for susceptible isolates")
	parser.add_argument("-a", "--anti", required = True, help = "Antibiotic name desired to predict")
	parser.add_argument("-f", "--frag", type = int, help = "Fragment size, default is the whole genome")
	parser.add_argument("-k", "--kfold", type = int, help = "Number of folds in CV")
	parser.add_argument("-t", "--thres", type = float, help = "Threshold for clustering")
	parser.add_argument("-s", "--strt", required = True, help = "List of the desired starting points")
	parser.add_argument("-w", "--clw", default = 1, type = int, help = "Select option 1 or 2 for clustering method. 1) Whole-alignment based clustering 2) Sub-alignment based clustering")
	parser.add_argument("-c", "--clus", type = str, required = True, help = "Provide the path for the clustering results. The format is dependent on the option was selected for --clw option. For option 1)Provide the full path for the clustering file -including the file name-, 2)Provide the path for the directory that includes all the clustering files per sub-alignment. The program requires the files named 'cluster_StartPosition_Threshold.txt', the StartPosition stands for the start position of the sub-alignment and Threshold stands for the k-mer similarity threshold used for clustering.")

	args = parser.parse_args()

	ant_name = args.anti

	fragment_size = args.frag

	threshold = args.thres

	clusters_path = args.clus

	cv = args.kfold

	clustering_way = args.clw

	def cv_ind_weight(clusters_path, data_z, cv):

		##this function is required for finding the indexes for cv and input weights for ML

		##clustered samples

		cluster_dict = fragments.clusterm(clusters_path)

		##test/training clusters and samples

		remaining_clusters = list(cluster_dict.keys())

		training_data_clustered = []

		r = int(len(remaining_clusters)/cv)
		a = 0
		b = 0
		for i in range(cv):

			if i != cv - 1:
				b = a + r
			else:
				b = len(remaining_clusters)

			training_data_clustered.append(remaining_clusters[a:b])

			a = a + r

		training_data_clustered_ind = []
		training_data_clustered_weights = []

		for c in training_data_clustered:
			temp = []
			temp2 = []
			for element in c:
				count = 0
				for item in cluster_dict[element]:	
					if item in data_z:
						ind = data_z.index(item)
						temp.append(ind)
						count = count + 1			
				for w in range(count): ##weight the samples based on the size of the clusters
					temp2.append(float(1)/count)

			training_data_clustered_ind.append(temp)
			training_data_clustered_weights.append(temp2)

		return training_data_clustered_ind, training_data_clustered_weights

	##input file names

	fasta_names_all = np.loadtxt(args.input, dtype = "string")

	##output data

	data_y_all = np.loadtxt(args.meta, dtype = "string", delimiter = "\t")

	##Prepare output data

	data_y_ant = fragments.export_meta(data_y_all, "%s" % ant_name)

	##starting points

	starting_points = np.loadtxt(args.strt, dtype = int)

	##fasta files that have valid outputs

	fasta_names, data_y_dict = fragments.meta(data_y_ant, fasta_names_all, args.res, args.sus)

	##read the input data

	fasta_list, data_z, size = fragments.read_data(fasta_names, args.directory)

	##output data

	data_y = fragments.prep_y(data_y_dict, data_z)

	del data_y_dict


	##For each sub-alignment, predict AMR

	if clustering_way == 1:

		##if the clustering was done using whole-alignmnet, splitting into training and test data will be happened only one time

		training_data_clustered_ind, training_data_clustered_weights = cv_ind_weight(clusters_path, data_z, cv)

	temp_performances_test = []

	starts = []

	if type(starting_points) is not list:

		starting_points = [starting_points]

	for start in starting_points:

		if clustering_way == 2:
			##if the clustering was done using sub-alignments, splitting into training and test data will be repeated per sub-alignment
			##because clusters are sub-alignment dependent

			training_data_clustered_ind, training_data_clustered_weights = cv_ind_weight("%s/cluster_%s_%s.txt" % (clusters_path, start, threshold), data_z, cv)

		##Prepare input matrix

		f_matrix = fragments.matrix(fasta_list, data_z, start, fragment_size)

		f_matrix_snp = fragments.snp_matrix(f_matrix)

		##If there is no variation

		if f_matrix_snp.shape[1] == 0:
			continue 

		del f_matrix

		##Encode the data 

		encoded_data_x = to_categorical(f_matrix_snp)

		encoded_data_x = np.reshape(np.ravel(encoded_data_x), (f_matrix_snp.shape[0], f_matrix_snp.shape[1]*encoded_data_x.shape[2]))

		del f_matrix_snp

		##Machine learning 

		perf = []

		for i in range(cv):

			test_index = training_data_clustered_ind[i]

			remain = list(set(range(cv)) - set([i]))

			train_index = []

			for c in remain:
				for item in training_data_clustered_ind[c]:
					train_index.append(item)

			weights = []

			for c in remain:
				for item in training_data_clustered_weights[c]:
					weights.append(item)

			train_data_x, test_data_x = encoded_data_x[train_index], encoded_data_x[test_index]
			train_data_y, test_data_y = np.array(data_y)[train_index], np.array(data_y)[test_index]

			clf_1 = RandomForestClassifier(n_estimators = 200, random_state = 42, class_weight = "balanced", n_jobs = 2)
			clf_1.fit(train_data_x, train_data_y, sample_weight = weights)

			predictions_test = clf_1.predict_proba(test_data_x)

			try:

				test_prob = predictions_test[:,1]
				test_class = clf_1.predict(test_data_x)

				##Test performances

				##FPR, TPR

				fpr, tpr, _ = roc_curve(test_data_y, test_prob, pos_label = 1)

				perf.append([fpr, tpr])
			except IndexError:
				print("Index error")

		##If there was an index error

		if len(perf) < 5: 
			continue
		else:
			temp_performances_test.append(perf)

		starts.append(start)

	np.save("test_5K_%s_clustered_by_fragments_%s" % (ant_name, str(threshold)), temp_performances_test)	
	np.save("start_5K_%s_clustered_by_fragments_%s" % (ant_name, str(threshold)), starts)

