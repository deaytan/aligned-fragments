# Getting Started #

```
git clone https://bitbucket.org/deaytan/aligned-fragments.git
```

# Predicting AMR from subalignments using Random Forests #

In the repository, you will find the scripts to predict antimicrobial resistance (AMR) from partial global alignments. Using these scripts, the machine learning models 
in the study by Aytan-Aktug et al. (2021) can be reproduced, moreover, these models can be applicable to new studies for predicting AMR using sub-alignments. 

The Subalignments are subsampled from a whole chromosomes that are aligned against a reference chromosome, although the models would work for other 
type of organisms as well.  The alignment process is necessary for sampling the same genetic content across different isolates 
We suggest KMA as an aligner, however, other alignment tools would also work. 

To predict AMR from sub-alignments, the user should decide the size of the sub-alignment. In the study by Aytan-Aktug et al. (2021), sub-alignments ranged in size from 1Kb to 1Mb. Once the sub-alignment size is decided, the program will pick a random region to generate the sub-alignment. By setting a random seed, 
we make sure that these sub-alignments are reproducible. Moreover, the predictions with sub-alignments will be repeated multiple times dependent on the sub-alignment size. 
The number of models is calculated as the length of the alignment divided by the sub-alignment length. Each sample will correspond a row in the input matrix and 
the positions in the sub-alignment will be columns in the input matrix. Since the machine learning models can handle the numeric data in most cases only, the entries in the matrix are one-hot encoded, 
in other words each nucleotide in the sub-alignment is encoded with ones and zeros. 

The generated input matrix, or possibly a part of it, can be used to feed the machine learning model. In the scripts, the implemented machine learning is random forests. 
We chose to apply random forests due to model simplicity. Our random forest model is applied for classification purposes and, this program is only able to predict binary 
classes such as resistant or susceptible. 

To tune and validate the random forest models, our program can pick a random hold out dataset, and it can use the rest of the data for K-fold CV model. 

To evaluate the model predictions, we used multiple performance assessment measures such as Area Under Curve (AUC), F1 score, Matthews correlation coefficient (MCC), accuracy, major error (ME) rate 
and very major error rate (VME). 

To avoid the learning from genome similarity in the machine learning models, we put an option as called clustering. Using clustering option, the program keeps the isolates 
 that share genomic similarity for a given threshold in the same partitions such as hold out, test or training sets. The genome similarities should be calculated using KMA program which calculates k-mer distances.  

### KMA ###

In this study, KMA was used for alignment purpose using the following parameters: -dense -ref_fsa -ca -mem_mode -mrs 0 -Mt1 1 -e 1.0.

KMA index was used for Hobohm-1 clustering, to detect the similar isolates for a given threshold using the following parameters: -k 16 -Sparse - -NI.

KMA is available here:

https://bitbucket.org/genomicepidemiology/kma/src/master/

### Make_sub_alignments.py ###

This script selects random sub-alignments which will be clustered using KMA. The program includes two steps: the first step picks the sub-alignments and write them into one 
file as KMA required; the second step calls kma index program for clustering for a given similarity threshold. The program generates number of sub-alignments until the total size of the 
sub-alignments cover the whole-alignment size. By picking the sub-alignment size as the whole-alignment size, one can have the clustering results 
for the whole-alignments. The program provides merged sub-alignments into single files, clustering results and list of the used sub-alignment starting points. 

usage: make_sub_alignments.py [-h] [-f FRAG] [-t THRES] [-i INPUT] [-d DIRECTORY]

optional arguments:


  -h, --help  show this help message and exit
  
  
  -f FRAG, --frag FRAG  Sub-alignment size
  
  
  -t THRES, --thres THRES  Threshold for clustering
  
  
  -i INPUT, --input INPUT  List of the isolates required
  
  
  -d DIRECTORY, --directory DIRECTORY  Path to the aligned isolates directory

### Random_forest_model.py ###

Random_forest_model.py generates fragment or whole genome matrix. This matrix was one hot-encoded to convert all the nucleotides 
to numeric values. Random forest models was trained, validated and tested by the sub-matrixes. All the predictions were assessed by using various methods: AUC, F1, accuracy, MCC, ME, VME.
The model is able to cluster the isolates based on the genome similarity and keeps the similar isolates into the same CV partitions. For that option, isolate clusters 
should be generated using KMA beforehand. 

usage: random_forest_model.py [-h] [-i INPUT] [-d DIRECTORY] [-m META] [-r RES] [-u SUS] [-a ANTI] [-f FRAG] [-c CLUS] [-k KFOLD] 

optional arguments:


  -h, --help          show this help message and exit
  
  
  -i INPUT, --input   INPUT List of the isolates required
  
  
  -d DIRECTORY, --directory DIRECTORY Path to the aligned isolates directory
  
  
  -m META, --meta META  Metadata
  
  
  -r RES, --res RES     Value for resistant isolates
  
  
  -u SUS, --sus SUS     Value for susceptible isolates
  
  
  -a ANTI, --anti ANTI  Antibiotic name desired to predict

  
  -f FRAG, --frag FRAG  Fragment size, default is the whole genome
  
  
  -c CLUS, --clus CLUS  For clustering, provide clustering results (whole alignment only)
  
  
  -k KFOLD, --kfold KFOLD  Number of folds in CV
 

### RF_with_clustered_subalignments.py

RF_with_clustered_subalignments.py runs a random forest model to predict AMR with clustered sub-alignments based on sub-alignments or originated whole-alignment k-mer similarity. To do that, beside the input sub-alignments, the program requires KMA index clustering results to cluster sub-alignments. 
Once the program started, the program will split the sub-alignments into the test and training sets based on the k-mer similarity of sub-alignments or originated whole-alignments. The similar sub-alignments for a given threshold kept in the same 
partitions such as test or training data sets where the learning from genome similarity is reduced. This program will save the False Negative Rates and True Positive Rates per starting point of the sub-alignment.

RF_with_clustered_subalignments.py [-h] [-i INPUT] [-d DIRECTORY] [-m META] [-r RES] [-u SUS] [-a  ANTI] [-f FRAG]  [-c CLUS]  [-w CLW] [-k KFOLD]  [-t THRES] [-s STRT]

optional arguments:
  -h,  --help  show this help message and exit
  
  
  -i INPUT, --input INPUT  List of the isolates required
  
  
  -d DIRECTORY, --directory DIRECTORY  Path to the aligned isolates directory
  
  
  -m META, --meta META  Metadata
  
  
  -r RES, --res RES     Value for resistant isolates
  
  
  -u SUS, --sus SUS     Value for susceptible isolates
  
  
  -a ANTI, --anti ANTI  Antibiotic name desired to predict
  
  
  -f FRAG, --frag FRAG  Fragment size, default is the whole genome
  
  
  -k KFOLD, --kfold KFOLD  Number of folds in CV
  
  
  -t THRES, --thres THRES  Threshold for clustering
  
  
  -s STRT, --strt STRT  List of the desired starting points
  
  
  -w CLW, --clw CLW     Select option 1 or 2 for clustering method. 1) Whole-
                        alignment based clustering 2) Sub-alignment based
                        clustering.
                        
                        
  -c CLUS, --clus CLUS  Provide the path for the clustering results. The
                        format is dependent on the option was selected for
                        --clw option. For option 1)Provide the full path for
                        the clustering file -including the file name-,
                        2)Provide the path for the directory that includes all
                        the clustering files per sub-alignment. The program
                        requires the files named
                        'cluster_StartPosition_Threshold.txt', the
                        StartPosition stands for the start position of the
                        sub-alignment and Threshold stands for the k-mer
                        similarity threshold used for clustering.

### Fragments.py ###

Fragments.py is a Python module. It includes all the necessary functions to generate matrix, find columns with no variation,
preparing metadata etc. Therefore, it should be loaded prior to run any other scripts.

### Examples with the K. pneumoniae toy dataset ###

To cluster whole genome alignments or subalignments:

python make_sub_alignments.py -f 5000000 -t 0.90 -i ./example_dataset/example_dataset.txt -d ./example_dataset

To run random forest model with subalignments:

python random_forest_model.py -i ./example_dataset/example_dataset.txt -d ./example_dataset -m ./example_dataset/KPN.sir.filt.tab -r "4" -u "1" -a ciprofloxacin -f 5000000 -k 5

To run random forest model with clustered subalignments:

python RF_with_clustered_subalignments.py -i ./example_dataset/example_dataset.txt -d ./example_dataset -m ./example_dataset/KPN.sir.filt.tab -r "4" -u "1" -a ciprofloxacin -f 500000 -k 5 -c ./example_dataset/fragment_files/ -w 2 -t 0.90 -s ./example_dataset/fragment_files/starting_points.txt

### Pre-required python packages and programs ###

Python packages:

* Sklearn
* Collecctions
* Numpy
* Matplotlib
* Random
* Keras
* Argparse 
* Os
* Sys

Programs:

* KMA (version:1.2.0)
* KMA index for clustering using Hobohm-1 algorithm (version:1.3.7)