#!/usr/bin/env python2

import os
os.environ["OMP_NUM_THREADS"] = "2" 
import argparse
import fragments
import random
import numpy as np
import sys

if(__name__ == "__main__"):
	random.seed(42)
	
	parser = argparse.ArgumentParser()
	
	parser.add_argument("-f", "--frag", required= True, type = int, help = "Sub-alignment size")
	parser.add_argument("-t", "--thres", required = True, type = float, help = "Threshold for clustering")
	parser.add_argument("-i", "--input", required = True, help = "List of the isolates required")
	parser.add_argument("-d", "--directory", default = ".", help = "Path to the aligned isolates directory")
	
	args = parser.parse_args()
	
	fragment_size = args.frag
	
	threshold = args.thres
	
	fasta_names_all = np.loadtxt(args.input, dtype = "string")
	
	#random.shuffle(fasta_names_all)
	
	##create a directory for the output
	
	os.system("mkdir fragment_files")
	
	##Read the input data
	
	fasta_list, data_z, size = fragments.read_data(fasta_names_all, args.directory)
	
	#random.shuffle(data_z)
	
	##Total length of the sub-samples should cover the whole-alignment size
	
	sampling_size = int(size)/fragment_size
	
	##Write the starting points into a file
	
	output = open("./fragment_files/starting_points.txt", "w")
	
	for s in range(sampling_size):
		
		##random starting point
		
		start = fragments.find_region(len(fasta_list[data_z[0]]), fragment_size)
		
		##write the starting point into the file
		
		output.write(str(start) + "\n")
		
		##merge the sub-alignments and write into a single file 
		
		f_matrix = fragments.listed(fasta_list, data_z, start, fragment_size)
		
		nrow = len(f_matrix)
		
		filem = open("./fragment_files/%s_5K_%s.fna" % (start, str(threshold)), "w")
		
		for n in range(nrow):
			filem.write(">%s\n" % data_z[n])
			filem.write(f_matrix[n] + "\n")
		
		filem.close()
		
		##call KMA index for Hobohm-1 clustering
		cmd = "cat ./fragment_files/%s_5K_%s.fna | kma index -i -- -k 16 -Sparse - -ht %f -hq %f -NI -o ./fragment_files/delete_%s 1> ./fragment_files/cluster_%s_%s.txt" % (start, str(threshold), threshold, threshold, str(threshold), start, str(threshold))
		os_errno = os.system(cmd)
		if(os_errno != 0):
			sys.stderr.write("Failed Cmd:\t%s\n" %(cmd))
			sys.exit(os_errno)
		
	output.close()
