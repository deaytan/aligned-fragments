##This module includes all the functions needed. 

import random
import numpy as np
import collections
import matplotlib.pyplot as plt

def find_region(fasta_len, fragment_size):

	##find a random starting point

	start = random.randint(0, fasta_len - fragment_size)

	return start 


def matrix(fasta_list, data_z, start, fragment_size):

	##generate an empty matriz and 
	##fill with the nucleotides

	f_matrix = np.full((len(data_z), fragment_size), -1)
	
	for i in range(len(data_z)):

		for item in range(fragment_size):

			output = converter(fasta_list[data_z[i]][start+item])

			f_matrix[i, item] = output
			
	return f_matrix
			

def snp_matrix(f_matrix):

	##check each column whether has a variation or not
	##if there is no variation, discard it 

	nrow, ncol = f_matrix.shape[0], f_matrix.shape[1]

	indexes = []

	for i in range(ncol):
		entr = f_matrix[:, i]

		if len(list(set(list(entr)))) != 1:
			indexes.append(i)

	return f_matrix[:,indexes]

def meta(data_y, fasta_names_all, res, sus):

	##prepare a dict including metadata
	##it works for PATRIC IDs such as 1234.567


	data_y_dict = collections.defaultdict(list)

	for i in data_y:
		if res in i[1]:
			out = 1
			data_y_dict[i[0]] = out
		elif sus in i[1]:
			out = 0
			data_y_dict[i[0]] = out

	overlap = []

	#random.shuffle(fasta_names_all)

	for i in fasta_names_all:

		i_updated = i.replace(".fsa", "")
		i_updated = i_updated.replace(".fna", "")

		if i_updated in data_y_dict:
			if data_y_dict[i_updated] == 1 or data_y_dict[i_updated] == 0:
				overlap.append(i)
	#random.shuffle(overlap)

	return overlap, data_y_dict

def prep_y(data_y_dict, data_z):

	##generate true outputs

	data_y = []

	for i in data_z:
		out = data_y_dict[i]

		data_y.append(out)

	return data_y


def read_data(fasta_names, directory):

	##read the inputs and generate a dict

	fasta_dict = collections.defaultdict(list)

	data_z = []

	for i in fasta_names:

		temp = []

		f_open = open("%s/%s" % (directory, i), "r")

		f_reads = f_open.readlines()

		if len(f_reads[1:]) != 0: ##if the input data is not empty

			i_updated = i.replace(".fsa", "")
			i_updated = i_updated.replace(".fna", "")

			data_z.append(i_updated)

			for f in f_reads[1:]:
				temp.append(f.rstrip('\n'))

			fasta_dict[i_updated] = "".join(temp)

			f_open.close()
	
	return fasta_dict, data_z, len(fasta_dict[i_updated])

def converter(nucl):

	##Convert nucleotides to numeric values 

	if nucl.lower() == "a":
		return 0
	elif nucl.lower() == "c":
		return 1
	elif nucl.lower() == "g":
		return 2
	elif nucl.lower() == "t":
		return 3
	elif nucl == "n":
		return 4 
	elif nucl == "N":
		return 5
	else:
		print("unrecognized nucleotide")

def bar_plot(mean_values, std, y_label, plot_label):

	##visualize the averaged results

	plt.bar(np.arange(len(mean_values)), mean_values, yerr = std)
	plt.xlabel("fragment sizes")
	plt.ylabel(y_label)
	plt.title("Fragments vs Performances")
	plt.xticks(np.arange(len(mean_values)), fragment_sizes)
	plt.savefig("./%s.png" % plot_label)
	plt.close()

def mean_std(all_performances):

	##Calculate the mean performances with a standard deviation

	mean_values = float(sum(all_performances))/len(all_performances)
	std = np.std(all_performances)

	return [mean_values, std]

def class_finder(prob, threshold, res, sus):

	##calculate the classes based on thee class probabilities
	classes = []

	for i in prob:
		if i > threshold:
			classes.append(res)
		else:
			classes.append(sus)
	return classes

def listed(fasta_list, data_z, start, fragment_size):

	###this function lists all the sub-alignments into single file. Required for clustering. 

	listm = []
		
	for i in range(len(data_z)):
						
		fragment = fasta_list[data_z[i]][start:start+fragment_size]

		fragment = fragment.replace("n", "-")

		listm.append(fragment)
															
	return listm

def ME(y_true, y_pred, res, sus):

	##calculate major error
	##when the sample is non-resistant, but predicted resistant
	me = 0
	for i in range(len(y_true)):
		if y_true[i] == 0 and y_pred[i] == 1:
			me = me + 1
	return float(me)/len(y_true)

def VME(y_true, y_pred, res, sus):

	##calculate verj major error
	##when the sample is resistant, but predicted non-resistant 
	vme = 0
	for i in range(len(y_true)):
		if y_true[i] == 1 and y_pred[i] == 0:
			vme = vme + 1
	return float(vme)/len(y_true)

def export_meta(data_y, antibiotic):

	##export the metadata of the desired antibiotic 

	output = []

	for each in data_y:
		if antibiotic.lower() in each[1].lower():
			output.append([each[0], each[2]])
	return output
		

def calculate_identity(seq1, seq2):
	
	##calculate identity between the query and template 
	
	countm = 0
	for i in range(len(seq1)):
		if seq1[i] == seq2[i]:
			countm = countm + 1

	return countm, countm/float(len(seq1))
	
	
def clusterm(stdout):

	##make a dict, keys are cluster numbers, items are cluster members. 

	dictm = collections.defaultdict(list)
	out_o = open(stdout, "r")
	out = out_o.readlines()

	for i in out:
		if "." in i:
			i = i.split("\t")
			idm = i[0]
			cls = int(i[1])

			dictm[cls].append(idm)
	out_o.close()
	return dictm
